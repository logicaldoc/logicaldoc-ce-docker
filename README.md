# Quick reference

-   **Where to get help**:
    [the LogicalDOC Community Forums](https://forums.logicaldoc.com/), [the Docker Community Forums](https://forums.docker.com/), [the Docker Community Slack](https://blog.docker.com/2016/11/introducing-docker-community-directory-docker-community-slack/)

-   **Where to file issues**:
    [https://gitlab.com/logicaldoc/logicaldoc-ce-docker/issues](https://gitlab.com/logicaldoc/logicaldoc-ce-docker/issues)

-   **Supported Docker versions**:
    [the latest release](https://github.com/docker/docker/releases/latest) (down to 1.12 on a best-effort basis)
    
-   **Guides and user manuals**:
    [LogicalDOC Documentation](https://docs.logicaldoc.com)    

# What is LogicalDOC DMS - Community Edition?

[LogicalDOC DMS - Community Edition](https://www.logicaldoc.com/download-logicaldoc-community), often simply "LogicalDOC CE", is a web-based document management system designed to efficiently manage large document archives.

As a document manager its main purpose is to store, introspect, and categorize files, with a strong emphasis on preserving the contextual and business information of documents. It also provide means to ease retrieval, as requested by users or other software applications, be it those on the same computer or those running on another computer across a network (including the Internet). It can handle workloads ranging from small single-machine applications to large enterprise applications with many concurrent users. It can also preview, label and send documents. Other features of interest are its group based access control, SOAP and REST API.

> [wikipedia.org/wiki/LogicalDOC](https://en.wikipedia.org/wiki/LogicalDOC)

![logo](https://www.logicaldoc.com/images/assets/LogicalDocWhiteH02-167.png)

# How to use this image

## Start a LogicalDOC DMS instance

With Docker properly installed, proceed to download the LogicalDOC DMS image using the command:

```console
$ docker pull logicaldoc/logicaldoc-ce-docker
```

Simple start 
```console 
$ docker run -d --name logicaldoc-dms -p 8080:8080 --link mysql-ld logicaldoc/logicaldoc-ce-docker
```
Note: the first start could be long, to get a better idea of when it ends it might be easier to start the process without the -d option

Start as a daemon with attached volumes to persist documents and settings
```console
$ docker run -d --name logicaldoc-dms --restart=always -p 8080:8080 -v logicaldoc-conf:/opt/logicaldoc/conf -v logicaldoc-repo:/opt/logicaldoc/repository logicaldoc/logicaldoc-ce-docker
```

The container will be available by browsing to [http://localhost:8080](http://localhost:8080)

All document files will be stored in the volume ``logicaldoc-repo``, the configuration files insead are in volume ``logicaldoc-conf`` 

If another web server is running on port 8080 use a different port in the ``-p`` option, ie: ``-p 8081:8080``.

## Start with an external database

Run the MySQL container
```Shell
docker run -d --name=mysql-ld --env="MYSQL_ROOT_PASSWORD=mypassword" --env="MYSQL_DATABASE=logicaldoc" --env="MYSQL_USER=ldoc" --env="MYSQL_PASSWORD=changeme" mysql
```

Start with a link to the external MySQL container
```console 
$ docker run -d --name logicaldoc-dms -p 8080:8080 --env="DB_ENGINE=mysql" --link mysql-ld logicaldoc/logicaldoc-ce-docker
```
Note: the first start could be long, to get a better idea of when it ends it can be wise to start the process without the -d option

## Stopping and starting the container

To stop the container use:

```console
$ docker stop logicaldoc-dms
```

To start the container again:

```console
$ docker start logicaldoc-dms
```

## Configuration

To edit the settings file, check the physical location of the ``logicaldoc-conf`` volume using:

```console
$ docker volume inspect logicaldoc-conf
```

Which should produce an output similar to this one:

```console
    [
        {
            "Name": "logicaldoc-conf",
            "Driver": "local",
            "Mountpoint": "/var/lib/docker/volumes/logicaldoc-conf/_data",
            "Labels": null,
            "Scope": "local"
        }
    ]
```

In this case the physical location of the ``logicaldoc-conf`` volume is ``/var/lib/docker/volumes/logicaldoc-conf/_data``. 


## Environment Variables

The LogicalDOC DMS image uses several environment variables. While none of the variables are required, they may significantly aid you in using the image.

### `DB_ENGINE`

Defaults to `embedded`. This environment variable configures the database backend to use. The database backends supported by this Docker image are: embedded, mysql, postgres
Note: you can also use a MariaDB container, and in this case the right configuration of DB_ENGINE variable is mysql 

### `DB_NAME`

Defaults to `logicaldoc`. This optional environment variable can be used to define the database name that LogicalDOC DMS will connect to. 

### `DB_HOST`

Defaults to `mysql-ld`. This optional environment variable is used to set the hostname that will be used to connect to the database. This can be the hostname of another container or an IP address.

### `DB_USER`

Defaults to `ldoc`. This optional environment variable is used to set the username that will be used to connect to the database. 

### `DB_PASSWORD`

Defaults to `changeme`. This optional environment variable is used to set the password that will be used to connect to the database. 

### `DB_PORT`

Defaults to `3306`. This optional environment variable is used to set the port number to use when connecting to the database.

### `LDOC_MEMORY`

Defaults to `2000`. Memory allocated for LogicalDOC expressed in MB. 


## Performing backups

To backup the existing data, check the physical location of the ``logicaldoc-conf`` and ``logicaldoc-repo`` volumes using:

```console
$ docker volume inspect logicaldoc-conf
```

Which should produce an output similar to this one:

```console
    [
        {
            "Name": "logicaldoc-conf",
            "Driver": "local",
            "Mountpoint": "/var/lib/docker/volumes/logicaldoc-conf/_data",
            "Labels": null,
            "Scope": "local"
        }
    ]
```

```console
$ sudo tar -zcvf backup.tar.gz /var/lib/docker/volumes/logicaldoc-conf/_data /var/lib/docker/volumes/logicaldoc-repo/_data
$ sudo chown `whoami` backup.tar.gz
```

If an external PostgreSQL or MySQL database or database containers, these too need to be backed up using their respective procedures.


## Restoring from a backup

Uncompress the backup archive in the original docker volume using:

```console
$ sudo tar -xvzf backup.tar.gz -C /
```

## Building the image

Clone the repository with:

```console
$ git clone https://gitlab.com/logicaldoc/logicaldoc-ce-docker.git
```

Change to the directory of the cloned repository:

```console
$ cd logicaldoc-ce-docker
```

Execute Docker's build command:

```console
$ docker build -t logicaldoc/logicaldoc-ce-docker .
```

Or using an apt cacher to speed up the build:

```console
$ docker build -t logicaldoc/logicaldoc-ce-docker --build-arg APT_PROXY=172.18.0.1:3142 .
```

Replace the IP address `172.18.0.1` with the IP address of the Docker host used from which these commands are running.


## Testing

Start a Vagrant box from the include Vagrant file. This Vagrant box will builds the Docker image and then start a container:

```console
$ vagrant up
```

Create the same Vagrant box using an apt cacher to speed up the build:

```console
$ APT_PROXY=172.18.0.1:3142 vagrant up
```

Replace the IP address `172.18.0.1` with the IP address of the Docker host used from which these commands are running.

## Using Docker compose

To deploy a complete production stack using the included Docker compose file execute:

```console
$ docker-compose -f docker-compose.yml up -d
```

This Docker compose file will provision 2 containers:

- MySQL as the database
- LogicalDOC CE using the above service container

To stop the stack use:

```console
$ docker-compose -f docker-compose.yml stop
```

The stack will also create three volumes to store the data of each container. These are:

- ldoc_conf - The LogicalDOC configuration container, normally called `logicaldoc-conf` when not using Docker compose.
- ldoc_repository - The LogicalDOC DMS data container for documents and search indexes, normally called `logicaldoc-repo` when not using Docker compose.
- ldoc_db - The database volume, in this case MySQL.

### Stopping and starting with Docker compose 

To stop the services use:

```console
$ docker-compose -f docker-compose.yml stop
```

To start the services again:

```console
$ docker-compose -f docker-compose.yml start
```

To remove the stopped containers:

```console
$ docker-compose -f docker-compose.yml rm -v
```

Destroys the containers and all the created volumes:

```console
$ docker-compose -f docker-compose.yml down -v
```
### Docker compose examples

Some docker-compose examples are available in the repository of this container on GitLab https://gitlab.com/logicaldoc/logicaldoc-ce-docker

## ... via [`docker stack deploy`](https://docs.docker.com/engine/reference/commandline/stack_deploy/) or [`docker-compose`](https://github.com/docker/compose)

Example `stack.yml` for `logicaldoc-dms`:

```yaml
version: "3.1"

services:

  logicaldoc:
    depends_on:
      - mysql-ld
    command: ["./wait-for-it.sh", "mysql-ld:3306", "-t", "30", "--", "/opt/logicaldoc/start-logicaldoc.sh", "run"]
    image: logicaldoc/logicaldoc-ce-docker
    ports:
      - 8080:8080
    environment:
      - LDOC_MEMORY=2000
      - DB_ENGINE=mysql

  mysql-ld: 
    image: mysql:5.7
    environment:
      - MYSQL_ROOT_PASSWORD=example
      - MYSQL_DATABASE=logicaldoc
      - MYSQL_USER=ldoc
      - MYSQL_PASSWORD=changeme
      
```

[![Try in PWD](https://github.com/play-with-docker/stacks/raw/cff22438cb4195ace27f9b15784bbb497047afa7/assets/images/button.png)](http://play-with-docker.com?stack=https://gitlab.com/logicaldoc/logicaldoc-ce-docker/raw/master/stack.yml)

Run `docker stack deploy -c stack.yml logicaldocdms` , wait for it to initialize completely, and visit `http://swarm-ip:8080`, `http://localhost:8080`, or `http://host-ip:8080` (as appropriate).
